SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';


-- -----------------------------------------------------
-- Table `post_category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `{$prfx}post_category` ;

CREATE  TABLE IF NOT EXISTS `{$prfx}post_category` (
  `{$prfx}post_category_id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `object_type_id` TINYINT UNSIGNED NOT NULL ,
  `parent_id` MEDIUMINT UNSIGNED NULL ,
  `name` VARCHAR(255) NULL ,
  `uri` VARCHAR(255) NULL ,
  `title` VARCHAR(255) NULL ,
  `is_published` TINYINT(1) NOT NULL DEFAULT 1 ,
  `sort_order` MEDIUMINT UNSIGNED NULL ,
  PRIMARY KEY (`{$prfx}post_category_id`) ,
  INDEX `fk_{$prfx}post_category_parent_id_idx` (`parent_id` ASC) ,
  INDEX `fk_{$prfx}post_category_object_type_id_idx` (`object_type_id` ASC) ,
  CONSTRAINT `fk_{$prfx}post_category_post_category_id`
    FOREIGN KEY (`parent_id` )
    REFERENCES `{$prfx}post_category` (`{$prfx}post_category_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE
--  CONSTRAINT `fk_{$prfx}post_category_object_type_id`
--    FOREIGN KEY (`object_type_id` )
--    REFERENCES `{$prfx}object_type` (`{$prfx}object_type_id` )
--    ON DELETE CASCADE
--    ON UPDATE CASCADE
) ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `post`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `{$prfx}post` ;

CREATE  TABLE IF NOT EXISTS `{$prfx}post` (
  `{$prfx}post_id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `object_type_id` TINYINT UNSIGNED NOT NULL ,
  `object_id` INT UNSIGNED NULL ,
  `{$prfx}post_category_id` MEDIUMINT UNSIGNED NULL ,
  `status_id` TINYINT NOT NULL ,
  `name` VARCHAR(255) NULL ,
  `uri` VARCHAR(255) NULL ,
  `title` VARCHAR(255) NOT NULL ,
  `intro` TEXT NULL ,
  `content` TEXT NULL ,
  `publish_date` INT NULL ,
  `creation_time` INT NOT NULL ,
  `creator_id` INT UNSIGNED NULL ,
  `author_id` INT UNSIGNED NULL ,
  PRIMARY KEY (`{$prfx}post_id`) ,
  INDEX `fk_{$prfx}post_object_id_idx` (`object_id` ASC) ,
  INDEX `fk_{$prfx}post_object_type_id_idx` (`object_type_id` ASC) ,
  INDEX `fk_{$prfx}post_user_id_idx` (`creator_id` ASC) ,
  INDEX `fk_{$prfx}post_{$prfx}post_category_id_idx` (`{$prfx}post_category_id` ASC) ,
  INDEX `fk_{$prfx}post_author_id_idx` (`author_id` ASC) ,
--  CONSTRAINT `fk_{$prfx}post_object_id`
--    FOREIGN KEY (`object_id` )
--    REFERENCES `{$prfx}object` (`{$prfx}object_id` )
--    ON DELETE CASCADE
--    ON UPDATE CASCADE,
--  CONSTRAINT `fk_{$prfx}post_object_type_id`
--    FOREIGN KEY (`object_type_id` )
--    REFERENCES `{$prfx}object_type` (`{$prfx}object_type_id` )
--    ON DELETE CASCADE
--    ON UPDATE CASCADE,
  CONSTRAINT `fk_{$prfx}post_user_id`
    FOREIGN KEY (`creator_id` )
    REFERENCES `{$prfx}user` (`{$prfx}user_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_{$prfx}post_{$prfx}post_category_id`
    FOREIGN KEY (`{$prfx}post_category_id` )
    REFERENCES `{$prfx}post_category` (`{$prfx}post_category_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_{$prfx}post_author_id`
    FOREIGN KEY (`author_id` )
    REFERENCES `{$prfx}person` (`{$prfx}person_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
