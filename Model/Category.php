<?php

namespace Core\Sitedev\Posts\Model;

use App\ActiveRecord\Tree;
use App\Cms\Ext\File;

abstract class Category extends Tree
{
    /** @var Category */
    protected $_parent;

    /** @var Category[] */
    protected $_children;

    public function __construct()
    {
        parent::__construct('post_category');

        $this->addPrimaryKey('integer');
        $this->addAttr('object_type_id', 'integer');
        $this->addAttr('parent_id', 'integer');
        $this->addAttr('name', 'string');
        $this->addAttr('uri', 'string');
        $this->addAttr('title', 'string');
        $this->addAttr('is_published', 'boolean');
        $this->addAttr('sort_order', 'integer');
    }

    protected function _checkAndFillName()
    {
        if (!$this->name)
            $this->name = File::normalizeName($this->title);
    }

    public function create()
    {
        if (!$this->_objectTypeId)
            $this->_objectTypeId = static::OBJECT_TYPE_ID;

        $this->uri = $this->computeUri();

        return parent::create();
    }

    public function update()
    {
        $uri = $this->computeUri();

        if ($this->uri != $uri) {
            $this->uri = $uri;
            $this->rebuildUris();
        }

        return parent::update();
    }

    public static function getList($_where = null, $_params = null)
    {
        $where = empty($_where) ? array() : $_where;
        $where['object_type_id'] = static::OBJECT_TYPE_ID;

        return parent::getList($where, $_params);
    }

    /**
     * @return Category
     */
    public function getParent()
    {
        if (!isset($this->_parent)) {
            if ($this->parentId) {
                $this->setParent(static::getById($this->parentId));

            } else $this->setParent(false);
        }

        return $this->_parent;
    }

    public function setParent($_parent)
    {
        $this->_parent = $_parent;
    }

    public function computeUri()
    {
        $this->_checkAndFillName();
        $uri  = $this->getParent() ? rtrim($this->getParent()->uri, '/') : '';
        $uri .= '/' . $this->name;
        return $uri;
    }

    /**
     * @param bool $_force
     * @return Category[]
     */
    public function getChildren($_force = false)
    {
        if (is_null($this->_children) || $_force)
            $this->setChildren(static::getList(['parent_id' => $this->getId()]));

        return $this->_children;
    }

    /**
     * @param Category[] $_children
     */
    public function setChildren($_children)
    {
        $this->_children = $_children;

        foreach ($this->_children as $child)
            $child->setParent($this);
    }

    public function rebuildUris()
    {
        $this->rebuildBranchUris($this->getChildren(), $this);
    }

    /**
     * @param Category[] $_items
     * @param Category $_parent
     */
    public function rebuildBranchUris($_items, $_parent)
    {
        foreach ($_items as $entity) {
            $entity->setParent($_parent);
            $uri = $entity->computeUri();

            if ($entity->uri != $uri) {
                $entity->updateAttr('uri', $uri);
                $entity->rebuildUris();
            }
        }
    }

    public function isPublished()
    {
        return (bool) $this->isPublished;
    }

    /**
     * @param string $_node
     * @param string|array $_xml
     * @param array $_attrs
     * @return string
     */
    public function getXml($_node = null, $_xml = null, $_attrs = null)
    {
        $attrs = is_array($_attrs) ? $_attrs : [];

        if (!array_key_exists('uri', $attrs))
            $attrs['uri'] = $this->uri;

        return parent::getXml($_node, $_xml, $attrs);
    }
}
