<?php

namespace Core\Sitedev\Posts\Model;

use App\ActiveRecord;
use App\Cms\Back\Office\NavFilter;
use App\Cms\Back\Office\NavFilter\Element;
use App\Cms\Ext\File;
use App\Cms\User;
use Ext\Date;

abstract class Post extends ActiveRecord
{
    const HIDDEN    = 0;
    const DRAFT     = 1;
    const PUBLISHED = 2;
    const ARCHIVE   = 3;
    const REJECTED  = 4;

    /** @var array */
    protected static $_statuses;

    public function __construct($_table = null)
    {
        parent::__construct(empty($_table) ? 'post' : $_table);

        $this->addPrimaryKey('integer');
        $this->addAttr('object_type_id', 'integer');
        $this->addForeign(User::createInstance(), 'creator_id');
        $this->addAttr('status_id', 'integer');
        $this->addAttr('name', 'string');
        $this->addAttr('title', 'string');
        $this->addAttr('intro', 'text');
        $this->addAttr('content', 'text');
        $this->addAttr('publish_date', 'integer');
        $this->addAttr('creation_time', 'integer');
    }

    /**
     * @return array
     */
    public static function getStatuses()
    {
        if (!isset(static::$_statuses)) {
            static::$_statuses = array(
                static::HIDDEN    => array('title' => 'скрыто'),
                static::DRAFT     => array('title' => 'черновик'),
                static::PUBLISHED => array('title' => 'опубликовано'),
                static::ARCHIVE   => array('title' => 'в архиве'),
                static::REJECTED  => array('title' => 'забраковано')
            );

            foreach (array_keys(static::$_statuses) as $id) {
                static::$_statuses[$id]['id'] = $id;
            }
        }

        return static::$_statuses;
    }

    /**
     * @return bool
     */
    public function isPublished()
    {
        return $this->statusId == static::PUBLISHED;
    }

    /**
     * @return string
     */
    public function getIntro()
    {
        return $this->intro;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @return int
     */
    public function getPublishDate()
    {
        return $this->publishDate;
    }

    public function checkAndFillName()
    {
        if (!$this->name)
            $this->name = File::normalizeName($this->title);
    }

    public function create()
    {
        if (!$this->_objectTypeId)
            $this->_objectTypeId = static::OBJECT_TYPE_ID;

        $this->checkAndFillName();

        return parent::create();
    }

    public function update()
    {
        $this->checkAndFillName();
        return parent::update();
    }

    /**
     * @param array $_where
     * @param array $_params
     * @return self[]
     */
    public static function getList($_where = null, $_params = null)
    {
        $where = empty($_where) ? array() : $_where;
        $where['object_type_id'] = static::OBJECT_TYPE_ID;

        $params = empty($_params) ? array() : $_params;
        if (!isset($params['order']))
            $params['order'] = 'publish_date DESC';

        return parent::getList($where, $params);
    }

    public static function getCount($_where = null)
    {
        $where = empty($_where) ? array() : $_where;
        $where['object_type_id'] = static::OBJECT_TYPE_ID;

        return parent::getCount($where);
    }

    public function fillWithData(array $_data)
    {
        $data = $_data;

        if (!empty($data['publish_date'])) {
            $this->publishDate = Date::getDate($data['publish_date']);
            unset($data['publish_date']);
        }

        parent::fillWithData($data);
    }

    public function getBackOfficeXml($_xml = array(), $_attrs = array())
    {
        $attrs = $_attrs;

        if (!isset($attrs['is-published']))
            $attrs['is-published'] = $this->statusId == static::PUBLISHED;

        return parent::getBackOfficeXml($_xml, $attrs);
    }

    /**
     * @return NavFilter
     */
    public static function getCmsNavFilter()
    {
        $filter = new NavFilter(get_called_class());


        // Дата

        $filter->addElement(new Element\Date(
            'publish_date',
            'integer',
            'Дата публикации'
        ));


        // Заголовок

        $filter->addElement(new Element('title', 'Название'));


        // Статус

        $statuses = static::getStatuses();

        if (count($statuses) > 0) {
            $el = new Element\Multiple('status_id', 'Статус');

            foreach ($statuses as $item)
                $el->addOption($item['id'], $item['title']);

            $filter->addElement($el);
        }


        $filter->run();
        return $filter;
    }
}
