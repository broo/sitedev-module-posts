<?php

require_once '../../../../../www/cms/prepend.php';
// use Sitedev\Posts\Model;
// Чтобы IDE не ругалось:
use Core\Sitedev\Posts\Model;
use Core\Sitedev\Persons\Model as Persons;
use Core\Sitedev\Tags\Model as Tags;
use Core\Sitedev\Sources\Model as Sources;

$page = new App_Cms_Back_Page();

if ($page->isAllowed()) {

    // Инициализация объекта

    $obj = null;

    if (!empty($_GET['id'])) {
        $obj = Model\Post::getById($_GET['id']);
        if (!$obj) reload();

    } else if (key_exists('add', $_GET)) {
        $obj = new Model\Post();
    }


    // Форма редактирования или добавления объекта

    if ($obj) {
        $form = App_Cms_Ext_Form::load(dirname(__FILE__) . '/form.xml');
        $form->fillWithObject($obj);

        foreach (Model\Post::getStatuses() as $status) {
            $form->statusId->addOption($status['id'], $status['title']);
        }


        $form->authorId->addOption(null, 'Без автора');

        foreach (Persons\Person::getList() as $author) {
            $form->authorId->addOption($author->id, $author->getTitle());
        }


        $sourceTypes = Sources\Type::getList();

        if ($sourceTypes) {
            foreach ($sourceTypes as $type) {
                $form->getGroup('sources')->addElement(
                    $form->createElement(
                        'source-type-' . $type->id,
                        'string',
                        $type->getTitle()
                    )
                );
            }

            foreach ($obj->getSources() as $source) {
                $el = $form->getElement('source-type-' . $source->sourceTypeId);

                if ($el) {
                    $el->setValue($source->value);
                }
            }

        } else {
            $form->deleteGroup('sources');
        }


        foreach (Tags\Tag::getList() as $tag) {
            $form->tagIds->addOption($tag->id, $tag->getTitle());
        }

        if ($obj->id) {
            $form->tagIds->setValue($obj->getTagIds());

        } else {
            $form->publishDate->setValue(time());
        }

        $form->run();

        if ($form->isSubmited() && $form->isSuccess()) {
            if ($form->isSubmited('delete')) {
                $obj->delete();

                App_Cms_Back_Log::logModule(
                    App_Cms_Back_Log::ACT_DELETE,
                    $obj->id,
                    $obj->getTitle()
                );

                App_Cms_Ext_Form::saveCookieStatus();
                redirect($page->getUrl('path'));

            } else {
                $obj->fillWithData($form->toArray());
                $obj->save();


                // Обновление тэгов

                $tagIds = $form->tagIds->getValue();
                if (!$tagIds) $tagIds = array();

                foreach (
                    Ext_String::split($form->additionalTags->getValue()) as
                    $item
                ) {
                    $tagIds[] = Tags\Tag::getOrCreateIfUnique($item)->id;
                }

                $obj->updateTags(array_values(array_unique($tagIds)));


                // Обновление ресурсов

                if ($sourceTypes) {
                    $sources = array();

                    foreach ($sourceTypes as $type) {
                        $el = $form->getElement('source-type-' . $type->id);

                        if ($el && $el->getValue()) {
                            $sources[] = array(
                                'type_id' => $type->id,
                                'value' => $el->getValue()
                            );
                        }
                    }

                    $obj->updateSources($sources);
                }


                App_Cms_Back_Log::logModule(
                    $form->isSubmited('insert') ? App_Cms_Back_Log::ACT_CREATE : App_Cms_Back_Log::ACT_MODIFY,
                    $obj->id,
                    $obj->getTitle()
                );

                App_Cms_Ext_Form::saveCookieStatus();
                reload('?id=' . $obj->id);
            }
        }
    }


    // Статус обработки формы

    $formStatusXml = '';

    if (!isset($form) || !$form->isSubmited()) {
        $formStatusXml = App_Cms_Ext_Form::getCookieStatusXml(
            empty($obj) ? 'Выполнено' : 'Данные сохранены'
        );

        App_Cms_Ext_Form::clearCookieStatus();
    }


    // XML модуля

    $xml = Model\Post::getCmsNavFilter()->getXml() . $formStatusXml;
    $attrs = array('type' => 'simple', 'is-able-to-add' => 'true');

    if (empty($obj)) {
        if (App_Cms_Back_Section::get()->description) {
            $xml .= Ext_Xml::notEmptyNode('content', Ext_Xml::cdata(
                'html',
                '<p class="first">' . App_Cms_Back_Section::get()->description . '</p>'
            ));
        }

    } else if ($obj->getId()) {
        $attrs['id'] = $obj->id;
        $xml .= Ext_Xml::cdata('title', $obj->getTitle());
        $xml .= $form->getXml();

    } else {
        $attrs['is-new'] = 1;
        $xml .= Ext_Xml::cdata('title', 'Добавление');
        $xml .= $form->getXml();
    }

    $page->addContent(Ext_Xml::node('module', $xml, $attrs));
}

$page->output();
